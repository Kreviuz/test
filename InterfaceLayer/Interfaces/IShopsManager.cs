﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InterfaceLayer.Models;

namespace InterfaceLayer.Interfaces
{
    public interface IShopsManager
    {
        Task<List<ShopModelDto>> GetShops();
        Task<List<ProductModelDto>> GetProductsByShopId(long shopId);
    }
}
