﻿using System;

namespace InterfaceLayer.Models
{
    public class ShopModelDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Schedule { get; set; }
    }
}
