﻿namespace InterfaceLayer.Models
{
    public class RelationModelDto
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public long ProductId { get; set; }
    }
}
