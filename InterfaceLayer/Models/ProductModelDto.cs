﻿namespace InterfaceLayer.Models
{
    public class ProductModelDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
