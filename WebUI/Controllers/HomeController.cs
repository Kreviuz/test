﻿using InterfaceLayer.Interfaces;
using InterfaceLayer.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IShopsManager _manager;

        public HomeController(IShopsManager manager)
        {
            _manager = manager;
        }

        public async Task<ViewResult> Index()
        {
            var modelDto = await _manager.GetShops();
            var model = modelDto.Select(AutoMapper.Mapper.Map<ShopModelDto, ShopModel>);
            return View(model);
        }

        public async Task<ViewResult> GetProducts(long id)
        {
            var modelDto = await _manager.GetProductsByShopId(id);
            var model = modelDto.Select(AutoMapper.Mapper.Map<ProductModelDto, ProductModel>);
            return View(model);
        }
    }
}