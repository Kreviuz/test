﻿namespace WebUI.Models
{
    public class ProductModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}