﻿namespace WebUI.Models
{
    public class ShopModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Schedule { get; set; }
    }
}