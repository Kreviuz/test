﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.Helpers
{
    public static class CryptHelper
    {
        private static readonly char[] Characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".ToCharArray();

        public static string GenerateSalt()
        {
            var cryptProvider = new RNGCryptoServiceProvider();
            var rnd32 = new byte[32];
            cryptProvider.GetNonZeroBytes(rnd32);
            var result = new StringBuilder(rnd32.Length);
            foreach (var b in rnd32)
            {
                result.Append(Characters[b % (Characters.Length)]);
            }
            return result.ToString();
        }

        public static string HashPasswordWithSalt(string password, string salt)
        {
            var hasher = new SHA512Managed();
            var passwordHashArray = hasher.ComputeHash(Encoding.Default.GetBytes(password));
            var passwordHash = HashByteArrayToString(passwordHashArray);
            var passWordHashWithSalt = passwordHash + salt;
            var resultHash = hasher.ComputeHash(Encoding.Default.GetBytes(passWordHashWithSalt));
            var result = HashByteArrayToString(resultHash);
            return result;
        }

        private static string HashByteArrayToString(byte[] array)
        {
            var result = new StringBuilder(array.Length);
            foreach (byte t in array)
            {
                result.Append(t.ToString("x2"));
            }
            return result.ToString();
        }
    }
}
