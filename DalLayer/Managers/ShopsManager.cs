﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InterfaceLayer.Interfaces;
using InterfaceLayer.Models;
using DalLayer.Repositories;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;

namespace DalLayer.Managers
{
    public class ShopsManager : IShopsManager
    {
        private ShopsRepositiry _shopsRepo;
        private ProductsRepository _productsRepo;
        private RelationRepository _relationRepo;

        public Task<List<ShopModelDto>> GetShops()
        {
            var shopsEntity = _shopsRepo.GetShopsAsync();
            return shopsEntity;
        }

        public async Task<List<ProductModelDto>> GetProductsByShopId(long shopId)
        {
            var products = await (from rel in _relationRepo._relationContext.Relations
                                  join s in _shopsRepo._shopsContext.Shops on rel.ShopId equals s.Id
                                  join p in _productsRepo._productsContext.Products on rel.ProductId equals p.Id
                                  where rel.Id == shopId
                                  select p).ToListAsync();
            return products;
        }
    }
}
