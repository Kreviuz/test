﻿using InterfaceLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalLayer.Interfaces
{
    public interface IRelationRepositiry : IDisposable
    {
        Task<List<RelationModelDto>> GetRelations();
        Task SaveAsync();
    }
}
