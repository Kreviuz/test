﻿using InterfaceLayer.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DalLayer.Interfaces
{
    public interface IShopsRepository : IDisposable
    {
        Task<List<ShopModelDto>> GetShopsAsync();
        Task SaveAsync();
    }
}
