﻿using System;
using System.Threading.Tasks;
using InterfaceLayer.Models;
using System.Collections.Generic;

namespace DalLayer.Interfaces
{
    public interface IProductsRepository : IDisposable
    {
        Task Save();
    }
}
