﻿using System.Data.Entity;
using System.Threading.Tasks;
using DalLayer.Contexts;
using DalLayer.Interfaces;
using InterfaceLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace DalLayer.Repositories
{
    public class ProductsRepository : IProductsRepository
    {
        public readonly ProductsContext _productsContext;

        public ProductsRepository()
        {
            _productsContext = new ProductsContext();
        }

        public void Dispose()
        {
            _productsContext.Dispose();
        }

        public async Task<List<ProductModelDto>> GetProductsByShopId(long id)
        {
            var product = await _productsContext.Products.Where(x => x.Id == id).ToListAsync();
            return product;
        }

        public async Task Save()
        {
            await _productsContext.SaveChangesAsync();
        }
    }
}
