﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using DalLayer.Contexts;
using DalLayer.Interfaces;
using InterfaceLayer.Models;

namespace DalLayer.Repositories
{
    public class ShopsRepositiry : IShopsRepository
    {
       public readonly ShopContext _shopsContext;

       public ShopsRepositiry()
        {
            _shopsContext = new ShopContext();
        }

        public void Dispose()
        {
            _shopsContext.Dispose();
        }

        public async Task<List<ShopModelDto>> GetShopsAsync()
        {
            return await _shopsContext.Shops.ToListAsync();
        }

        public async Task SaveAsync()
        {
            await _shopsContext.SaveChangesAsync();
        }
    }
}
