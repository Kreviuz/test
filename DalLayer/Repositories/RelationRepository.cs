﻿using System.Data.Entity;
using System.Threading.Tasks;
using DalLayer.Contexts;
using DalLayer.Interfaces;
using InterfaceLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace DalLayer.Repositories
{
    public class RelationRepository : IProductsRepository
    {
        public readonly RelationContext _relationContext;

        public RelationRepository()
        {
            _relationContext = new RelationContext();
        }

        public void Dispose()
        {
            _relationContext.Dispose();
        }

        public async Task Save()
        {
            await _relationContext.SaveChangesAsync();
        }
    }
}
