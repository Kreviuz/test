﻿using System.Data.Entity;
using InterfaceLayer.Models;

namespace DalLayer.Contexts
{
    public class ProductsContext : DbContext
    {
        public DbSet<ProductModelDto> Products { get; set; }
    }
}
