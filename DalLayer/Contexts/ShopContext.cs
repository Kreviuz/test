﻿using InterfaceLayer.Models;
using System.Data.Entity;

namespace DalLayer.Contexts
{
    public class ShopContext : DbContext
    {
        public DbSet<ShopModelDto> Shops { get; set; }
    }
}
