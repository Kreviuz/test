﻿using InterfaceLayer.Models;
using System.Data.Entity;

namespace DalLayer.Contexts
{
    public class RelationContext : DbContext
    {
        public DbSet<RelationModelDto> Relations { get; set; }
    }
}
